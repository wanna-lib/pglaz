package pglaz

type Configuration struct {
	Host string `json:"host"`
}

type (
	Json map[string]interface{}
)

type ErrorMessage struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

type (
	Context interface {
		Login(username string, password string, response *LoginResponse) Context
		Refresh(token string, response *RefreshResponse) Context
		GetCredit(token string, response *GetCreditResponse) Context
		GameList(token string, response *GameListResponse) Context
		AddCredit(token string, amount float64, response *AddCreditResponse) Context
		Launch(token string, gameCode string, response *LaunchResponse) Context
		GameSetting(token string, req GameSettingRequest, response *GameSettingResponse) Context
		DeleteHistory(token string, response *DeleteHistoryResponse) Context
		Error() error
	}
	context struct {
		conf Configuration
		err  error
	}
)

func (c context) handleError(err error) Context {
	c.err = err
	return &c
}

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		conf: conf,
	}
}
