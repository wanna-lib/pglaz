package pglaz

import (
	"fmt"
	hpglaz "gitlab.com/wanna-lib/pglaz/helper"
	"testing"
)

const (
	username = "slmvp168"
	password = "Aa1682701."
)

var CTX = Init(Configuration{
	Host: "https://clients.pglaz.com",
})

func TestContext_Login(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(response)
}

func TestContext_Refresh(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseRefresh RefreshResponse
	if err := CTX.Refresh(response.Data.Authorization, &responseRefresh).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseRefresh)
}

func TestContext_GetCredit(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseGetCredit GetCreditResponse
	if err := CTX.GetCredit(response.Data.Authorization, &responseGetCredit).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseGetCredit)
}

func TestContext_GameList(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseGameList GameListResponse
	if err := CTX.GameList(response.Data.Authorization, &responseGameList).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseGameList)
}

func TestContext_AddCredit(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseAddCredit AddCreditResponse
	if err := CTX.AddCredit(response.Data.Authorization, 1000, &responseAddCredit).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseAddCredit)
}

func TestContext_Launch(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseLaunch LaunchResponse
	if err := CTX.Launch(response.Data.Authorization, "1402846", &responseLaunch).Error(); err != nil {
		panic(err)
	}
	fmt.Println(responseLaunch.Data.RedirectUrl)
	hpglaz.PrintStructJson(responseLaunch)
}

func TestContext_GameSetting(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseGameSetting GameSettingResponse
	if err := CTX.GameSetting(response.Data.Authorization, GameSettingRequest{
		GameCode:               "1402846",
		Name:                   "Midas Fortune",
		FeatureDemolowPay:      "0",
		FeatureDemonormalPay:   "0",
		FeatureDemoBigPay:      "0",
		FeatureDemoSuperBigPay: "0",
		DemoSuperBigPay:        "0",
		DemoBigPay:             "0",
		DemonormalPay:          "0",
		DemolowPay:             "10",
		NotPay:                 "90",
	}, &responseGameSetting).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseGameSetting)
}

func TestContext_DeleteHistory(t *testing.T) {
	var response LoginResponse
	if err := CTX.Login(username, password, &response).Error(); err != nil {
		panic(err)
	}

	var responseDeleteHistory DeleteHistoryResponse
	if err := CTX.DeleteHistory(response.Data.Authorization, &responseDeleteHistory).Error(); err != nil {
		panic(err)
	}
	hpglaz.PrintStructJson(responseDeleteHistory)
}
