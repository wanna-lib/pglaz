package pglaz

import (
	"bytes"
	"encoding/json"
	"fmt"
	requests "gitlab.com/trendgolibrary/trend-call"
	hpglaz "gitlab.com/wanna-lib/pglaz/helper"
)

type LaunchResponse struct {
	Success bool `json:"success"`
	Data    struct {
		RedirectUrl string `json:"redirect_url"`
	} `json:"data"`
}

func (c context) Launch(token string, gameCode string, response *LaunchResponse) Context {
	bodySignatureHeader := Json{
		`gameCode`: gameCode,
	}

	headers := map[string]string{
		"authority":          "clients.pglaz.com",
		"accept":             "application/json, text/plain, */*",
		"accept-language":    "en-US,en;q=0.9,th;q=0.8",
		"authorization":      "Bearer " + token,
		"content-type":       "application/json",
		"origin":             "https://www.pglaz.com",
		"referer":            "https://www.pglaz.com/",
		"sec-ch-ua":          "\"Google Chrome\";v=\"113\", \"Chromium\";v=\"113\", \"Not-A.Brand\";v=\"24\"",
		"sec-ch-ua-mobile":   "?0",
		"sec-ch-ua-platform": "\"macOS\"",
		"sec-fetch-dest":     "empty",
		"sec-fetch-mode":     "cors",
		"sec-fetch-site":     "same-site",
		"user-agent":         "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36",
	}

	bByte, _ := json.Marshal(bodySignatureHeader)
	reqParam := requests.Params{
		URL:     hpglaz.JoinStr(c.conf.Host, `/gwp6868/v1/launch`),
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bByte),
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
